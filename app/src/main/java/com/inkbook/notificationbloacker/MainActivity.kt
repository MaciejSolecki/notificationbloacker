package com.inkbook.notificationbloacker

import android.app.Notification
import android.app.NotificationManager
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    val TOTAL_TIME = 10000;
    val SLEEP_TIME = 50L;
    

    private val mNotificationManager : NotificationManager by lazy { getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager}
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }


    fun bloackNotications(view : View)
    {


        textView.text = "NOTIFICUTION THREAD RUNNING - CHECK NOTIFICATION LIST"
        button.isEnabled = false;

        Thread(Runnable {


            val totalNumber = TOTAL_TIME/SLEEP_TIME.toInt()
          //  notifyDownloadError(111,"TestOne","details");
            for( i in 0 until totalNumber)
            {
                notifyDownloadProgress(777,"Test Notifiaction", i , totalNumber)
                Thread.sleep(SLEEP_TIME);
            }


            notifyDownloadFinished(777,"Finished")
            Handler(Looper.getMainLooper()).post {
                textView.text = "";
                button.isEnabled = true;
            }

        }).start();
    }


    private fun notifyDownloadProgress(notificationId : Int, title : String, downloaded: Int, totalSize: Int) {


        Log.i("NOTIFICATIONBLOCKER", "Notification $downloaded");
        val builder = Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_file_download_black_24dp)
                .setContentTitle(title)
                .setOngoing(true)
                .setProgress(downloaded, totalSize, false)
                .setContentText("")

        mNotificationManager.notify(notificationId, builder.build())
    }


    private fun notifyDownloadFinished(notificationId : Int, title : String) {
        val builder = Notification.Builder(this)
            .setSmallIcon(R.drawable.ic_file_download_black_24dp)
            .setContentTitle(title)
            .setContentText("this.getString(R.string.download_complete)")
            .setProgress(0, 0, false)

        mNotificationManager.notify(notificationId, builder.build())
    }

    private fun notifyDownloadError(notificationId : Int, title : String, details : String) {
        val builder = Notification.Builder(this)
            .setSmallIcon(R.drawable.ic_file_download_black_24dp)
            .setContentTitle(title)
            .setContentText("this.getString(R.string.download_error)")
            .setProgress(0, 0, false)

        if(!TextUtils.isEmpty(details)) {
            builder.style = Notification.BigTextStyle().bigText(details)
        }

        mNotificationManager.notify(notificationId, builder.build())
    }
}